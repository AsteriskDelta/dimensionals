#include "PhyMeta.h"
#include <sstream>

namespace Modules {
    const std::string& PhyMeta::name() const {
        return meta.name;
    }
    void PhyMeta::setName(const std::string& nn) {
        this->meta.name = nn;
    }
        
    std::string PhyMeta::type() const {
        return "invalid";
    }
    
    std::string PhyMeta::ioType() const {
        if(this->isInput() && this->isOutput()) {
            return "duplex";
        } else if(this->isInput()) {
            return "input";
        } else if(this->isOutput()) {
            return "output";
        } else {
            return "undef";
        }
    }
        
    std::string PhyMeta::str() const {
        std::stringstream ss;
        ss << "[" << this->ioType() << "." << this->type() << "] " << this->name() << " = " << this->get() << "\n";
        return ss.str();
    }
};