#ifndef SIUNITS_H
#define SIUNITS_H
#include "Units.h"

#ifndef QUOTE
#define QUOTE(x) #x
#endif

//WARNING: ptr Factor(void*) WILL slice return values
#define PHYS_DECL(TYPE) \
template<class UNU = void>\
struct _##TYPE : Ins {\
    using Ins::Ins;\
    using Ins::From;\
    typedef _##TYPE<UNU> Self;\
    static std::string _used name;\
    static UnitClass _used *unitClass;\
    inline static UnitClass* cls() { return unitClass; };\
    inline static Self Factory() {\
        return Self();\
    }\
    inline static Ins InsFactory() {\
        return Self();\
    }\
    inline static Ins* PtrFactory(void *ptr);\
};\
template<class UNU> inline Ins* _##TYPE<UNU>::PtrFactory(void *ptr) {\
    if(ptr == nullptr) return static_cast<Ins*>(new _##TYPE<UNU>::Self());\
    else return static_cast<Ins*>( new(ptr) _##TYPE<UNU>::Self());\
}\
template<class UNU> std::string _##TYPE <UNU>::name = QUOTE(TYPE);\
template<class UNU> UnitClass* _##TYPE <UNU>::unitClass= AddUnitClass( _##TYPE <UNU>::name,\
&(_##TYPE<UNU>::InsFactory),\
&(_##TYPE<UNU>::PtrFactory));\
using TYPE = _##TYPE <>;

namespace Modules {
    namespace Units {
        PHYS_DECL(Reference);
        PHYS_DECL(Modifier);
        PHYS_DECL(Error);
        PHYS_DECL(Imaginary);
        
        PHYS_DECL(Mass);
        PHYS_DECL(Volume);
        PHYS_DECL(Pressure);

        PHYS_DECL(Area);
        PHYS_DECL(Length);
        PHYS_DECL(Angle);

        PHYS_DECL(Temperature);
        PHYS_DECL(EMField);
        PHYS_DECL(Radiation);

        PHYS_DECL(Voltage);
        PHYS_DECL(Wattage);
        PHYS_DECL(Amperage);
        PHYS_DECL(Resistance);

        PHYS_DECL(Frequency);
        PHYS_DECL(Time);

        PHYS_DECL(Force);
        PHYS_DECL(Energy);

        namespace SI {
            constexpr const unsigned int StandardModCnt = 17;
            extern Unit *yocto, *zepto, *atto, *femto, *pico, *nano, *micro, *milli, *base, *kilo, *mega, *giga, *tera, *peta, *exa, *zetta, *yotta;
            extern Unit **modList;
            extern Unit *deca;

            constexpr const signed int StartModPower = -24, EndModPower = 24, IncModPower = 3, IncModBase = 10;

            void InitSI();
        };
    };
}

#endif /* SIUNITS_H */

