#ifndef MIMOD_PHYMETA_H
#define MIMOD_PHYMETA_H
#include <iostream>
#include <string>
#include "MIMod.h"
#include "PhyModule.h"

namespace Modules {
    struct PhyMeta_Root {};
    
    class PhyMeta : public virtual PhyModule, private PhyMeta_Root {
    public:
        inline virtual ~PhyMeta() {};
        
        virtual const std::string& name() const;
        virtual void setName(const std::string& nn);
        
        virtual std::string type() const;
        virtual std::string ioType() const;
        
        virtual std::string str() const;
        inline virtual operator std::string() const {
            return this->str();
        }
    protected:
        struct Meta {
            std::string name;
        };
        Meta meta;
    };
    
    template<typename genType>
    typename std::enable_if<std::is_base_of<::Modules::PhyMeta_Root, genType>::value, std::ostream&>::type 
    inline operator<<(std::ostream& out, const genType& g) {
        return out << g.str();
    }
}

#endif



