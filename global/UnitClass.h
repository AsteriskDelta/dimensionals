#ifndef UNITCLASS_H
#define UNITCLASS_H
#include "MIMod.h"

namespace Modules {
    namespace Units {

        class UnitClass {
        public:
            UnitClass();
            UnitClass(const std::string& newName);
            virtual ~UnitClass();

            friend UnitClass* AddUnitClass(const std::string& name, InsFactoryFN factory, InsPtrFactoryFN ptrFactory);
            friend UnitClass* GetUnitClass(const std::string& name, bool create);

            inline const std::string& name() const {
                return raw.name;
            }
        protected:

            struct Raw {
                std::string name;
                InsFactoryFN factory;
                InsPtrFactoryFN ptrFactory;
            } raw;
        };
    };

};

#endif /* UNITCLASS_H */

