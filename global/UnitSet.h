#ifndef MOD_UNITSET_H

#define _UnitSet UnitSet
#define _UnitSetRaw UnitSetRaw

#ifdef MOD_UNITSET_H_2PASS
#define MOD_UNITSET_H
#define UnitSet UnitSetRaw
#define UType Num
#else
#define UType Ins
#endif
#include "MIMod.h"
#include "VectorArith.h"
#include "SmallMap.h"
#include "GenVector.h"
#include "Units.h"

namespace Modules {
    struct _UnitSet;
    struct _UnitSetRaw;
    
    struct UnitSet : SmallMap<ProtoPtr,UType>, VectorArith<UnitSet*>, GenVector {
        typedef SmallMap<ProtoPtr,UType> SMap;
        typedef VectorArith<UnitSet*> VA;
        
        UnitSet() : SMap(), VA(this), GenVector() {};
        UnitSet(const UnitSet& o) : SMap(o), VA(this), GenVector() {};
        
#ifndef MOD_UNITSET_H_2PASS
        template<typename T =_UnitSetRaw>
        inline T raw() const {
            T ret;
            for(SMap::Pair& pair : *this) {
                ret[pair.key] = pair.value.raw();
            }
            return ret;
        }
#endif
      
#ifdef MOD_UNITSET_H_2PASS
        template<typename T =_UnitSet>
        inline T whole() const {
            T ret;
            for(SMap::Pair& pair : *this) {
                ret[pair.key] = Ins::From(pair.key, pair.value);
            }
            return ret;
        }
#endif
    };
};

#undef UType

#ifndef MOD_UNITSET_H_2PASS
#define MOD_UNITSET_H_2PASS
#include "UnitSet.h"
#else
#undef UnitSet
#endif

#endif /* UNITSET_H */


