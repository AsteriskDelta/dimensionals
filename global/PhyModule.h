#ifndef MIMOD_PHYMOD_H
#define MIMOD_PHYMOD_H
#include "MIMod.h"
#include "PhyHistory.h"
#include "Units.h"

namespace Modules {
    class PhyModule : public PhyHistory {
    public:
        typedef UnitSet Value;
        
        inline virtual ~PhyModule() {
            this->terminate();
        };
        
        virtual bool initialize();
        virtual void terminate();
        
        virtual Ins get() const;
        inline Ins target() const {
            return const_cast<PhyModule*>(this)->target();
        }
        virtual Ins value();
        virtual Ins target();
        virtual bool set(const Ins& val);
                
        //From an external source
        virtual void wasRead(const Ins& val);
        
        virtual bool preupdate();
        virtual bool update();
        
        virtual bool setSampleRate(Num hertz);
        virtual const Num& sampleRateTarget() const;
        
        inline virtual bool isInput() const { return false; };
        inline virtual bool isOutput() const { return false; };
        inline virtual bool hidden() const { return false; };
        
        class Range {
            friend class PhyModule;
        public:
            void set(const Ins& mnn, const Ins& mxx);
            Value normalize(const Ins& val, bool raw = false);
            Ins operator()() const;
            
            void setCutoff(const Ins& val);
            Ins getCutoff() const;
            void setIncrement(const Ins& val);
            Ins getIncrement() const;
            /*
            void set(const Value& mnn, const Value& mxx);
            Value normalize(Value val, bool raw = false);
            
            inline Value operator()() const {
                return max - min;
            }*/
        protected:
            Value min, max;
            Value dead = 0.0;
            Value cutoff = 0.0;
            Value increment = 0.0;
            PhyModule *parentPtr;
            inline PhyModule *const& parent() const {
                return parentPtr;
            }
        };
        Range range;
        
    protected:
        
        virtual Value llget() const;
        inline Value lltarget() const {
            return const_cast<PhyModule*>(this)->lltarget();
        }
        
        virtual bool llset(const Value& val);
        
        virtual Value llvalidate(Value val);
        
        virtual Value& llvalue();
        inline Value llvalue() const {
            return const_cast<PhyModule*>(this)->llvalue();
        }
        virtual Value& lltarget();
        
        //From an external source
        virtual void llread(const Value& val);
        
        struct CTSet {
            Value current, target;
        };
        CTSet actual, constrained;
        
        Num desiredSampleRate;
    };
}

#endif


