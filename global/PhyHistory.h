#ifndef PHYHISTORY_H
#define PHYHISTORY_H
#include "MIMod.h"
#include "History.h"
#include "Units.h"
#include "UnitSet.h"

namespace Modules {

    class PhyHistory : protected History<UnitSet> {
    public:
        typedef History<UnitSet> HParent;
        
        PhyHistory();
        PhyHistory(const PhyHistory& orig);
        virtual ~PhyHistory();
        
        virtual void setUnit(const Ins& newUnit);
        
        virtual void add(const Ins& val, const Num& weight = 1.0);
        
        virtual Ins derivative(const Num& len) const;
        virtual Ins integral(const Num& len) const;
        virtual Ins mean(const Num& len) const;
        virtual Ins variance(const Num& len) const;
        
        virtual Ins range(const Num& len) const;
        
        Value sampleRate(const Num& len) const;
    protected:
        Ins internalUnit, externalUnit;
        bool extUnitTransform;
        
        Num conv(const Ins& val) const;
        Ins conv(const Num& val) const;
        inline void checkUnit(const Ins& val);
    };
};

#endif /* PHYHISTORY_H */

