#include "Units.h"
#include <cstring>
#include "SmallMap.impl.h"

template class SmallMap<unsigned int, Modules::Units::Conversion>;

namespace Modules {
    namespace Units {

        Ins::Ins() : Arith(), prototype() {
            
        }
        Ins::Ins(const Ins& o) : Arith(o), prototype(o.prototype) {
            
        }
        
        Ins::Ins(const ProtoPtr& proto, const Num& rawVal) : Arith(rawVal), prototype(proto) {
            
        }

        const Num& Ins::raw() const {
            return Arith::val;
        }
        Num& Ins::raw() {
            return Arith::val;
        }

        Ins Ins::as(const ProtoPtr& to) const {
            return Convert(*this, to);
        }

        std::string Ins::str(unsigned int digits) const {
            /*static thread_local char buffer[128];
            Num resolved;
            char *bufferPtr = &buffer[0];
            
            if(mod) {
                resolved = this->as(mod).raw();
            } else {
                resolved = this->raw();
            }
            
            int written = snprintf(bufferPtr, 128, "%.*lf", digits, resolved);
            if(written <= 0) return "[err]";
            else bufferPtr += written;
            
            if(mod) {
                memcpy(bufferPtr, mod->suffix().c_str(), mod->suffix().size()+1);
                bufferPtr += mod->suffix().size();
            }
            if(unit) {
                memcpy(bufferPtr, unit->suffix().c_str(), unit->suffix().size()+1);
                bufferPtr += unit->suffix().size();
            }
            return std::string(&buffer[0], static_cast<size_t>(bufferPtr - &buffer[0]));*/
        }
        
        Unit::Unit(const UnitClass *cls, const std::string& nm, const std::string& abbr) : name(nm), abbrev(abbr), unitClass(cls) {
            format.suffix = abbrev;
        }
            
        /*void Unit::setParent(ProtoPtr const& par, Num mult) {
            _unused(par); _unused(mult);
        }*/
        void Unit::setMultiplier(const Num& mult) {
            multiplier = mult;
        }


        int AddUnitClass(const UnitClass *cls) {

            return 0;
        }
        
        UnitClass* AddUnitClass(UnitClass *const& cls) {
            return cls;
        }

        UnitClass* GetUnitClass(const std::string& name, bool create) {

        }

        void AddUnit(const ProtoPtr& unit) {

        }

        ProtoPtr GetUnit(const std::string& name) {

        }

        void AddConversion(const ProtoPtr& from, const ProtoPtr & to, const Conversion& cv, bool bidirectional) {

        }

        Num Convert(const ProtoPtr & from, const ProtoPtr & to, const Num& val) {

        }

        Ins Convert(const Ins &val, const ProtoPtr & to) {

        }
        
        Ins Convert(const Ins &from, const Ins& to) {

        }
        
        Ins From(const ProtoPtr &proto, const Num& rawVal) {
            return Ins(proto, rawVal);
        }
        
        Ins From(const std::string& str) {
            _unused(str);
            return Ins();
        }
    }
}