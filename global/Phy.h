#ifndef PHY_H
#define PHY_H
#include "MIMod.h"
#include "PhyModule.h"
#include "PhyMeta.h"
#include "PhyDebug.h"

namespace Modules {
    struct Phy_Root {};

    class Phy : public virtual PhyModule, public PhyMeta, private Phy_Root {
    public:
        
        inline virtual ~Phy() {};
    };
};


#endif /* PHY_H */

