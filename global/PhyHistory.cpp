#include "PhyHistory.h"
#include "History.impl.h"

namespace Modules {
    template class History<UnitSet>;

    PhyHistory::PhyHistory() : HParent(), internalUnit(), externalUnit(), extUnitTransform(false) {
    }

    PhyHistory::PhyHistory(const PhyHistory& orig) : HParent(orig) {
    }

    PhyHistory::~PhyHistory() {
    }
    
    inline void PhyHistory::checkUnit(const Ins& val) {
        if(!internalUnit) this->setUnit(val);
    }

    void PhyHistory::setUnit(const Ins& newUnit) {
        if(!internalUnit) {
            internalUnit = newUnit;
            return;
        } else {
            externalUnit = newUnit;
            extUnitTransform = true;
        }
    }

    void PhyHistory::add(const Ins& val, const Num& weight) {
        checkUnit(val);
        HParent::push(this->conv(val), weight);
    }

    Ins PhyHistory::derivative(const Num& len) const {
        _unused(len);
        return this->conv(HParent::derivative());
    }
    Ins PhyHistory::integral(const Num& len) const {
        _unused(len);
        return this->conv(HParent::integral());
    }
    Ins PhyHistory::mean(const Num& len) const {
        _unused(len);
        return this->conv(HParent::mean());
    }
    Ins PhyHistory::variance(const Num& len) const {
        _unused(len);
        return this->conv(HParent::variance());
    }

    Ins PhyHistory::range(const Num& len) const {
        _unused(len);
        return this->conv(HParent::range());
    }

    PhyHistory::Value PhyHistory::sampleRate(const Num& len) const {
        _unused(len);
        return HParent::sampleRate();
    }

    Num PhyHistory::conv(const Ins& val) const {
        return Convert(val, &internalUnit).raw();
    }
    Ins PhyHistory::conv(const Num& val) const {
        if(extUnitTransform) {
            return Convert(From(&internalUnit, val), &externalUnit);
        } else {
            return From(&internalUnit, val);
        }
    }

};
