#ifndef UNITS_H
#define UNITS_H
#include "MIMod.h"
#include "SmallMap.h"
#include <ScalarArith.h>
#include <vector>
#include <cstddef>

namespace Modules {
    namespace Units {
        ProtoPtr GetUnit(const std::string& name);
        Ins From(const ProtoPtr &units, const Num& rawVal);
        
        struct ProtoPtr {
            Ins* prototype;
            
            inline ProtoPtr() : prototype(nullptr) {};
            inline ProtoPtr(const Unit *const& u);// : prototype(u) {};
            inline ProtoPtr(const std::string& unitName) : prototype(GetUnit(unitName)) {};
            inline ProtoPtr(const Ins *const& proto) : prototype(const_cast<Ins*>(proto)) {};
            
            inline explicit operator bool() const noexcept {
                return prototype != nullptr;
            }

            inline Ins* operator->() const {
                return prototype;
            }

            inline Ins& operator*() const {
                return *(this->operator->());
            }
            
            inline operator Ins*&() {
                return prototype;
            }
            inline operator Ins *const&() const {
                return prototype;
            }
        };

        struct Conversion {
            Num offset = 0.0;
            Num mult = 1.0;
            Num power = 1.0;
            Num logBase = 1.0;
            std::vector<ProtoPtr> *deps;

            inline ~Conversion() {
                if (deps != nullptr) delete deps;
            }
            void addDep(const ProtoPtr& proto);
        };

        /*typedef double UnitStore;

        class Ins : public SmallMap<UnitPtr, UnitStore>, public VectorArith<UnitIns*> {
        public:
            UnitStore as(const UnitPtrconst& to, const UnitPtrconst& mod = nullptr);
            
            const std::string& type(int idx = -1) const;
            std::string str(const UnitPtr mod, int idx = -1) const;
            inline std::string str(int idx = -1) const {
                return this->str(nullptr, idx);
            }
        };*/
        class Ins : public ScalarArith<Ins*, Num> {
        public:
            typedef ScalarArith<Ins*, Num> Arith;
            
            Ins();
            Ins(const ProtoPtr& proto, const Num& rawVal);
            Ins(const Ins& o);
            
            inline Ins& self() {
                return *this;
            }
            inline const Ins& self() const {
                return *this;
            }
            
            inline explicit operator bool() const {
                return flags.valid;
            }
            
            inline explicit operator Num() const {
                return this->raw();
            }
            
            ProtoPtr prototype;
            Ins *chain;
            struct Flags {
                union {
                    struct {
                        bool add : 1;
                        bool sub : 1;
                        bool mul : 1;
                        bool div : 1;
                        bool head : 1;
                        bool tail : 1;
                        bool valid : 1;
                    };
                    unsigned char raw;
                };
                inline Flags() : raw(0x0) {
                    //head = tail = true;
                }
            } flags;
            
            //Num raw(UnitPtr mod = nullptr) const;
            //Num asRaw(UnitPtr to, UnitPtr mod = nullptr) const;
            const Num& raw() const;
            Num& raw();
            
            Ins as(const ProtoPtr& to) const;
            
            const ProtoPtr& type() const {
                return prototype;
            }
            std::string str(unsigned int digits = 3) const;
            
            inline static Ins From(const ProtoPtr &proto, const Num& rawVal) {
                return ::Modules::Units::From(proto, rawVal);
            }
        };
        
        template<typename genType>
        typename std::enable_if<std::is_base_of<::Modules::Units::Ins, genType>::value, std::ostream&>::type 
        inline operator<<(std::ostream& out, const genType& g) {
            return out << g.str();
        }

        struct Unit : Ins {
            typedef SmallMap<unsigned int, Conversion> SMap;
            typedef SMap::Idt Idt;
            
            Idt id;
            std::string name, abbrev;
            const UnitClass *unitClass;

            Num multiplier = 1.0;

            //ProtoPtr parent = nullptr;
            SMap table;

            Unit(const UnitClass *cls, const std::string& nm, const std::string& abbr = "");
            
            //void setParent(const ProtoPtr & par, Num mult);
            void setMultiplier(const Num& mult);
            
            struct {
                std::string prefix = "", suffix = "";
            } format;
            
            inline const std::string& prefix() const {
                return format.prefix;
            }
            inline const std::string& suffix() const {
                return format.suffix;
            }
        };

        UnitClass* AddUnitClass(const std::string& name, InsFactoryFN factory = nullptr, InsPtrFactoryFN ptrFactory = nullptr);
        UnitClass* AddUnitClass(UnitClass *const& cls);
        UnitClass* GetUnitClass(const std::string& name, bool create = true);

        void AddUnit(const ProtoPtr& unit);
        ProtoPtr GetUnit(const std::string& name);
        
        void AddConversion(const ProtoPtr& from, const ProtoPtr& to, const Conversion& cv, bool bidirectional = true);

        //Num Convert(const ProtoPtr& from, const ProtoPtr& to, const Num& val);

        Ins From(const std::string& str);
        Ins From(const ProtoPtr &proto, const Num& rawVal);\
        Ins Convert(const Ins &val, const ProtoPtr& to);
        Ins Convert(const Ins &from, const Ins& to);
        
        inline ProtoPtr::ProtoPtr(const Unit *const& u) : prototype(const_cast<Unit *>(u)) {// : prototype(u) {};
            
        }

        /*namespace Temperature {
            UnitPtrKelvin, *Centigrade, *Fahrenheit;
        };

        namespace Pressure {
            UnitPtrPascal, *PSI, *mmHg;
        };

        namespace Mass {
            UnitPtrGram, *Pound;
        };

        namespace Volume {
            UnitPtrLiter, *Inch3, *Pint, *Gallon, *Quart;
        };

        namespace Electric {
            UnitPtrVolts, *Amps, *Watts;
        };
        namespace Magnetic {
            UnitPtrGauss;
        }


        using namespace Temperature;
        using namespace Pressure;
        using namespace Mass;
        using namespace Volume;
        using namespace Electric;
        using namespace Magnetic;
         * */
    };
    using namespace Units;
};

#endif /* UNITS_H */

