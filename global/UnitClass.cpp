#include "UnitClass.h"

namespace Modules {
    namespace Units {

        UnitClass::UnitClass() {
        }

        UnitClass::UnitClass(const std::string& newName) {
            raw.name = newName;
        }

        UnitClass::~UnitClass() {
        }
    };
};