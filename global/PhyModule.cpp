#include "PhyModule.h"

namespace Modules {
    bool PhyModule::initialize() {
        range.parentPtr = this;
        return true;
    }
    void PhyModule::terminate() {
        
    }
    
    
    /*
     * LOW-LEVEL PROTECTED FN
     */
    PhyModule::Value PhyModule::llget() const {
        return this->llvalue();
    }
        
    bool PhyModule::set(const Ins& val) {
        actual.target = this->conv(val);
        this->preupdate();
        
        const bool matches = constrained.target == actual.target;
        return matches;
    }
    
    void PhyModule::wasRead(const Ins& val) {
        actual.current = this->conv(val);
        this->preupdate();
    }
    
    Ins PhyModule::value() {
        return this->conv(constrained.current);
    }
    Ins PhyModule::target() {
        return this->conv(constrained.target);
    }
        
    Ins PhyModule::get() const {
        return this->conv(this->llvalue());
    }
        
    bool PhyModule::llset(const PhyModule::Value& val) {
        actual.target = val;
        this->preupdate();
        
        const bool matches = constrained.target == val;
        return matches;
    }
    
    void PhyModule::llread(const PhyModule::Value& val) {
        actual.current = val;
        this->preupdate();
    }
    
    PhyModule::Value& PhyModule::llvalue() {
        return constrained.current;
    }
    PhyModule::Value& PhyModule::lltarget() {
        return constrained.target;
    }
    
    bool PhyModule::preupdate() {
        bool insideCutoff = (range.cutoff > fabs(actual.target));
        
        if(this->isOutput() && !insideCutoff && (
                (range.dead == 0.0 || std::fabs(actual.target - constrained.target) > range.dead))) {
            constrained.target = this->llvalidate(actual.target);
        }
        
        if(!this->isInput()) {
            constrained.current = constrained.target;
        } else {
            constrained.current = this->llvalidate(actual.current);
        }
        
        return true;
    }
    
    bool PhyModule::update() {
        //TODO::Pass time to push() call
        HParent::push(this->llget());
        return true;
    }
    
    bool PhyModule::setSampleRate(Num hertz) {
        desiredSampleRate = hertz;
        return false;
    }
    
    const Num& PhyModule::sampleRateTarget() const {
        return desiredSampleRate;
    }

    void PhyModule::Range::set(const Ins& mnn, const Ins& mxx) {
        min = Convert(mnn, parent()->internalUnit).raw();
        max = Convert(mxx, parent()->internalUnit).raw();
    }
    PhyModule::Value PhyModule::Range::normalize(const Ins& insVal, bool raw) {
        Value val = Convert(insVal, parent()->internalUnit).raw();
        if(raw) return (val) / (max - min);
        else return (val - min) / (max - min);
    }
    
    PhyModule::Value PhyModule::llvalidate(PhyModule::Value val) {
        if(range.increment != 0.0) val = round(val / range.increment) * range.increment;
        val = std::max(std::min(range.max, val), range.min);
        return val;
    }
}
