#include "SIUnits.h"
namespace Modules {
    namespace Units {
        namespace SI {
            Unit *yocto, *zepto, *atto, *femto, *pico, *nano, *micro, *milli, *base, *kilo, *mega, *giga, *tera, *peta, *exa, *zetta, *yotta;
            Unit **modList;
            Unit *deca;

            std::string modNames[] = {"yocto", "zepto", "atto", "femto", "pico", "nano", "micro", "milli", "", "kilo", "mega", "giga", "tera", "peta", "exa", "zetta", "yotta"};
            std::string modAbbrevs[] = {"y", "z", "a", "f", "p", "n", "u", "m", "", "k", "M", "G", "T", "P", "E", "Z", "Y"};

            void InitSI() {
                modList = reinterpret_cast<Unit**> (&yocto);
                UnitClass *modCls = GetUnitClass("SI");
                AddUnitClass(modCls);

                int64_t power = StartModPower;

                for (unsigned int i = 0; i < StandardModCnt && power <= EndModPower; i++) {
                    Unit *& pfx = modList[i];
                    double mult = pow(double(IncModBase), double(power));

                    pfx = new Unit(modCls, modNames[i], modAbbrevs[i]);
                    pfx->setMultiplier(mult);

                    AddUnit(pfx);

                    power += IncModPower;
                }
            }
        }
    }
}
