NAME = Dimensionals
SYS_NAME = Dimensionals

#Directory that contains .cpp files with main()
EXEC_DIRS = ./tests/

#Directory that contains .dyn.txt files for dynamic libraries
DYN_SRC_DIRS = ./
#and .stc.txt for static ones
STATIC_SRC_DIRS = ./

#directories with classes/files to be compiled into objects
SRC_DIRS = lib classes tests

#directories containing headers that need to be installed
HEADER_DIRS = lib global
HEADER_CP_DIRS = actuators sensors subsys
HEADER_FILES += MIMod.h MIModules.h
#Header install subdirectory (ie, in /usr/include: defaults to $(SYS_NAME))
HEADERS_OUT_DIR = Dimensionals/

#Choose ONE header, if any, to precompile and cache (not for developement!!!)
#PCH = MIModule.h

#Default platform
TARGET_PLATFORM ?= Desktop
#Local build output directory
BUILD_DIR = build

#Compiler
CXX ?= g++
#CFLAGS (appended to required ones)
CXXPLUS ?= 
#SYS_FLAGS (prefix and possible override system CFLAGS, may break things)
CC_SYS_FLAGS ?=
#Optimization flags, supporting PGO if needed
OPTI ?= -O0
#Include paths, ie -I/path/to/headers/
INCPATH += 
#Libraries, ie -lopenmp
LIBS += 

include /usr/include/LPBT.mk

install::

uninstall::

autorun::

disable::

