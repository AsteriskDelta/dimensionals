#ifndef MIMOD_INC
#define MIMOD_INC

#include <iostream>
#include <string>
#include <cmath>
#include <cstdlib>
#include <limits>

#ifndef _unused
#define _unused(x) ((void)x)   
#endif

#ifndef weak
#define weak  __attribute__ ((weak))
#endif

#ifndef _used
#define _used __attribute__ ((used))
#endif

#ifndef _instantiator
#define _instantiator weak
#endif 

#ifndef _mutate
#define _mutate(FN, ARGS, VARS) \
inline auto& FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::add_lvalue_reference<typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type>::type;\
    return const_cast<RetType>(cthis->FN VARS);\
}
#endif

typedef double Num;

namespace Modules {
    namespace Units {
        struct Unit;
        struct Ins;
        struct ProtoPtr;
        class UnitClass;
        
        typedef Ins(*InsFactoryFN)();
        typedef Ins*(*InsPtrFactoryFN)(void*);
    };
    
    
};

template<typename KEY, typename VALUE>
class SmallMap;

#endif
