#ifndef MOD_ACTUATOR_INC
#define MOD_ACTUATOR_INC
#include "../MIModule.h"

namespace Modules {
    class Actuator : public Phy {
    public:
        
        inline virtual bool isOutput() const override { return true; };
    };
}

#endif
