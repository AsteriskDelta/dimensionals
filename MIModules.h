#ifndef MIMODULES_INC
#define MIMODULES_INC

#include "MIMod.h"

#include "Global.h"
#include "PhyDebug.h"
#include "PhyModule.h"

#include "sensors/Sensor.h"
#include "actuators/Actuator.h"

#endif