#ifndef MOD_THERMOMETER_INC
#define MOD_THERMOMETER_INC
#include "Sensor.h"

namespace Modules {
    class Thermometer : public Sensor {
    public:
        
        Temperature temperature() const;
        Temperature precision() const;
        
        bool active() const;
    };
}


#endif


