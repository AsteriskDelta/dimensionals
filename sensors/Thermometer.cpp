#include "Thermometer.h"

namespace Modules {
    Temperature Thermometer::temperature() const {
        return Temperature();
    }
    Temperature Thermometer::precision() const {
        return Temperature();
    }

    bool Thermometer::active() const {
        return true;
    }
    /*
    Num Thermometer::convertTo(const Degrees& oUnit, const Num& val) {
        if(oUnit == this->unit) return val;
        
        const unsigned char idx = static_cast<unsigned char>(unit),
                            cdx = static_cast<unsigned char>(oUnit);
        static const Num toKelvinMult[] = {1.0, 1.0, (5.0/9.0), 1.0};
        static const Num toKelvinOffset[] = {0.0, 273.15, 255.3722222};
        
        const Num valKelvin = toKelvinMult[idx] * val + toKelvinOffset[idx];
        
        return valKelvin / toKelvinMult[cdx] - toKelvinOffset[cdx];
    }*/
}
