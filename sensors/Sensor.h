#ifndef MOD_SENSOR_INC
#define MOD_SENSOR_INC
#include "../MIModule.h"

namespace Modules {
    class Sensor : public Phy {
    public:
        
        inline virtual bool isInput() const override { return true; };
    };
}


#endif

