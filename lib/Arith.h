#ifndef MOD_ARITH_H
#define MOD_ARITH_H
#include "MIMod.h"

namespace Modules {
    struct VectorArith_Root;
    struct ScalarArith_Root;
    
    template<typename T>
    T min(const T& a, const T& b, const std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        T ret;
        
        return ret;
    }
    template<typename T>
    T max(const T& a, const T& b, const std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        T ret;
        
        return ret;
    }
    /*
    template<typename T>
    T min(const T& a, const T& b, const std::enable_if<!std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        return std::min(a,b);
    }
    template<typename T>
    T max(const T& a, const T& b, const std::enable_if<!std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        return std::max(a,b);
    }*/
}

#endif
