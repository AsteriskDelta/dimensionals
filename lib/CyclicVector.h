#ifndef CYCLIC_VECTOR_H
#define CYCLIC_VECTOR_H
#include "GenIterator.h"
#include "MIMod.h"

template<typename V>
class CyclicVector {
public:
    typedef CyclicVector<V> Self;
    typedef V Value;
    typedef int Idt;
    typedef unsigned short Idx;
    struct iterator;
    
    CyclicVector();
    CyclicVector(Idt sz);
    
    const Value& get(const Idt& id) const;
    const Value& rawGet(const Idx& absID) const;
    _mutate(get,(const Idt& id),(id));
    _mutate(rawGet,(const Idt& id),(id));
    
    Idt push(const Value& val);
    Idt pop();
    
    Idt size() const;
    Idt maxSize() const;
    void setMaxSize(const Idx& sz);
    
    void clear();
    
    inline iterator nextToRecycle() {
        if(!this->saturated()) return this->invalid();
        else return this->end();
    }
    
    inline bool saturated() const {
        return edge.fill >= edge.max;
    }
    
    bool cycled() const;
    
    iterator begin() const;
    iterator end() const;
    iterator invalid() const;
protected:
    std::vector<Value> base;
    struct {
        Idx trailing = 0, leading = 0;
        Idx fill = 0, max = 0;
    } edge;
    
    inline Idx resolveIdt(const Idt& idt) const;
public:
    
    inline bool empty() const {
        return this->size() == 0;
    }
    
    struct iterator : GenIterator<V> {
        CyclicVector *vec;
        Idt idx;
        
        inline iterator(const Self *const& v, const Idt& i) :
        vec(const_cast<Self *const>(v)), idx(i) {};
        
        inline void advance() {
            idx++;
        }
        inline void retreat() {
            idx--;
        }
        inline Value* ptr() const {
            return &(vec->get(idx));
        }
    };
    
    inline const Value& operator[](const Idt& id) const {
        return this->get(id);
    }
    _mutate(operator[],(const Idt& id),(id))
    
    inline const Value& front() const {
        return this->get(Idt(edge.leading) - 1);
    }
    inline const Value& back() const {
        return this->get(edge.trailing);
    }
    _mutate(front,(),());
    _mutate(back,(),());
};

#endif
