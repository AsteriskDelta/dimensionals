#ifndef VECTORARITH_H
#define VECTORARITH_H
#include "MIMod.h"

#define VEC_TOK(x) x
#define VEC_CAT2(a,b) VEC_TOK(a b)
#define VEC_CAT(a,b) VEC_CAT2(VEC_TOK(a),VEC_TOK(b))
#define VEC_GEN_OP(OP, UNION_OP, INTER_OP, UNION, INTERSECTION) \
template<typename T>\
inline T VEC_CAT(operator,OP)(const T& o) const {\
    T ret;\
    for (auto it = parent->begin(); it != parent->end(); ++it) {\
        if(INTERSECTION && !o.has(it)) continue;\
        ret[it] =  (*it) OP (o[it]) ;\
    }\
    if(UNION) {\
        for (auto ot = o.begin(); ot != o.end(); ++ot) {\
            if (ret.has(ot)) continue;\
            ret[ot] = UNION_OP(*ot);\
        }\
    }\
    return ret;\
}
#define EQNAME2(NM) NM##=
#define EQNAME(NM) EQNAME2(NM)
#define VEC_OP_EQ(OP) \
template<typename T>\
inline auto& EQNAME(VEC_CAT(operator,OP))(const T& o) {\
    (*parent) = (*parent) OP (o);\
    return *parent;\
}

namespace Modules {
    struct VectorArith_Root{};

    template<typename PAR_T>
    struct VectorArith  : public VectorArith_Root {
        typedef PAR_T Parent;
        Parent parent;
        
        inline auto& self() const noexcept {
            return *parent;
        }
        
        inline VectorArith(Parent par) : parent(par) {};
        
        VEC_GEN_OP(+, ,+, true, false);
        VEC_GEN_OP(-,-,-, true, false);
        VEC_GEN_OP(*, ,*, false, true);
        VEC_GEN_OP(/, ,/, false, true);
        
        VEC_GEN_OP(&, ,;, false, true);
        VEC_GEN_OP(|, ,;, true, false);
        
        VEC_OP_EQ(+);
        VEC_OP_EQ(-);
        VEC_OP_EQ(*);
        VEC_OP_EQ(/);
        VEC_OP_EQ(&);
        VEC_OP_EQ(|);
        /*
        Num magnitude() const;
        Num magnitude2() const;
        
        Num distanceTo(const Vec &o) const;
        Num distanceTo2(const Vec &o) const;
        
        Num relDistanceTo(const Vec &o) const;
        Num relDistanceTo2(const Vec &o) const;
        
        Num dot(const Vec& ot) const;
        
        Vec& makeRelative(bool raw = false);
        Vec& clamp();
        
        Vec nonZero() const;
        
        inline explicit operator bool() const {
            return this->magnitude() > 0.001;
        }*/
        
        template<class=void>
        inline auto magnitude2() const noexcept {
            Num ret = 0.0;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret += static_cast<Num>((*it) * (*it));
            }
            return ret;
        }
        template<class=void>
        inline auto magnitude() const {
            return sqrt(this->magnitude2());
        }
        template<typename T>
        inline auto distanceTo2(const T& o) const {
            return (o - self()).magnitude2();
        }
        template<typename T>
        inline auto distanceTo(const T& o) const {
            return (o - self()).magnitude();
        }

        template<typename T>
        inline auto dot(const T& o) const noexcept {
            Num ret = 0.0;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret += static_cast<Num>((*it) * o[it.key]);
            }
            return ret;
        }
        
        template<class=void>
        inline auto mask() const noexcept {
            auto ret = *parent;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret[it.key] = 1.0;
            }
            return ret;
        }
        template<class=void>
        inline auto ones() const noexcept {
            return this->mask();
        }
        template<class=void>
        inline auto zeros() const noexcept {
            auto ret = *parent;
            for (auto it = parent->begin(); it != parent->end(); ++it) {
                ret[it.key] = 0.0;
            }
            return ret;
        }
    };

};

#endif /* VECTORARITH_H */

