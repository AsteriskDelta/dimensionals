#ifndef MOD_HISTORY_H
#ifdef MOD_HISTORY_H_2PASS
#define MOD_HISTORY_H
#define _virt virtual
#define History VirtHistory
#else
#define _virt 
#endif
#include "MIMod.h"
#include <list>
#include "CyclicVector.h"
#include <Cascade.h>

namespace Modules {
    template<typename VALUE> class History;
#ifndef MOD_HISTORY_H_2PASS
    template<typename HValue> struct HistorySample {
        HValue value;
        Num weight;
    };
#endif
    
    template<typename VALUE>
    class History : public CyclicVector<HistorySample<VALUE>>, public Cascade<History<VALUE>*> {
    public:
        typedef VALUE Value;
        typedef History<VALUE> Self;
        typedef CyclicVector<HistorySample<VALUE>> Cyclic;
        typedef Cascade<History<VALUE>*> Cascade_T;
        
        History();
        ~History();
        
        using Sample = HistorySample<VALUE>;
        
        struct {
            Num weight;
            unsigned int samples;
        } constraints;
        
        void setIdealWeight(const Num& weight);
        void setMaxSamples(const unsigned int& cnt);
        
        //Compute variance, sigma, and range (if needed)
        void frame();
        void clear();
        
        inline const Num& weight() const {
            return cache.weight;
        }
        
        inline void pop() {
            if(this->empty()) return;
            //running.duration = std::max(0.0, running.duration - data.front().duration / data.front().weight);
            this->pop();
        }

        void push(const Value& val, const Num& weight = 1.0) {
            typename Cyclic::iterator replaced = this->nextToRecycle();
            if(replaced) {
                this->account(replaced, -(replaced->weight));
            }
            
            this->push(val, weight);
            this->account(std::prev(this->end()), weight);
        }
        
        inline const Value& derivative() const {
            return cache.derivative;
        }
        inline const Value& integral() const {
            return cache.integral;
        }
        inline const Value& mean() const {
            return cache.mean;
        }
        inline const Value& variance() const {
            return cache.variance;
        }
        inline const Value& range() const {
            return cache.range;
        }
        
        //Normalized (via popiviciu's inequality) reciprocal of variance
        inline const Value& sigma() const {
            return cache.sigma;
        }
        
        //Distance of sigma from absolute noise (mag=0/axis) to absolute uniformity(mag=1/axis)
        inline const Num& consistence() const {
            return cache.consistence;
        }
        
        //Samples per unit weight
        inline Num sampleRate() const {
             return this->size() / this->weight();
        }
        
    protected:
        void computeRange();
        void computeVariance();
        
    private:
        struct {
            Num weight, consistence;
            Value mean;
            Value range, min, max;
            Value derivative, integral;
            Value variance, maxVariance;
            Value sigma;
            
            struct {
                bool range : 1;
                bool variance : 1;
                unsigned char padd : 6;
            } dirty;
        } cache;
        
        void account(typename Cyclic::iterator sample, bool additive);
    };
};

#undef _virt

#ifndef MOD_HISTORY_H_2PASS
#define MOD_HISTORY_H_2PASS
#include "History.h"
#else
#undef History
#endif

#endif /* HISTORY_H */

