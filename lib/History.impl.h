#include "History.h"

#ifndef MOD_HTORY_IMPL_H
#ifdef MOD_HTORY_IMPL_H_2PASS
#define MOD_HTORY_IMPL_H
#define History VirtHistory
#endif

#define HT_TPL template<typename VALUE>
#define HT_T History<VALUE>

#include "CyclicVector.impl.h"
#include "Cascade.impl.h"
#include "Arith.h"

namespace Modules {
    
    HT_TPL HT_T::History() : Cyclic(), Cascade_T(), cache() {
    }
    HT_TPL HT_T::~History()  {
    }
    
    HT_TPL
    void HT_T::setIdealWeight(const Num& weight) {
        constraints.weight = weight;
    }
    HT_TPL
    void HT_T::setMaxSamples(const unsigned int& cnt) {
        constraints.samples = cnt;
    }

    HT_TPL void HT_T::clear() {
        Cyclic::clear();
        this->cascade()->clear();
    }
    
    HT_TPL void HT_T::frame() {
        if(cache.dirty.range) this->computeRange();
        if(cache.dirty.variance) this->computeVariance();
    }
    
    HT_TPL void HT_T::computeRange() {
        //;//cache.min.clear(); cache.max.clear();
        //cache.min = std::numeric_limits<Value>::max();
        //cache.max = -std::numeric_limits<Value>::max();
        cache.min = cache.max = this->begin()->value;
        
        for(const Sample& sample : *this) {
            cache.min = min(cache.min, sample.value);
            cache.max = max(cache.max, sample.value);
        }
        
        cache.range = cache.max - cache.min;
        cache.maxVariance = cache.range * cache.range * 0.25;
        cache.dirty.range = false;
    }
    HT_TPL void HT_T::computeVariance() {
        if(cache.weight == 0) {
            cache.variance.clear();
            return;
        }
        
        Value totalVar, tmp;
        for(Sample& sample : *this) {
            tmp = (sample.value - cache.mean);
            totalVar += (tmp * tmp) * sample.weight;
            tmp.clear();
        }
        
        cache.variance = totalVar / cache.weight;
        
        //Reciprocal (mask = proto variance:1) of normalized variance (vec 0...1)
        cache.sigma = cache.variance.mask() - (cache.variance / cache.maxVariance);
        //Thus, a variance approaching 0 gives a sigma approaching 1
        
        cache.consistence = cache.sigma.magnitude() / 
                            (cache.sigma.ones() - cache.sigma.zeros()).magnitude();
        
        cache.dirty.variance = false;
    }
    
    HT_TPL void HT_T::account(typename HT_T::Cyclic::iterator sample, bool additive) {
        const Num mult = additive? 1.0 : -1.0;
        auto prevSample = std::prev(sample);
        
        cache.weight += sample->weight * mult;
        cache.mean += sample->value * (sample->weight/cache.weight * mult);
        cache.integral += sample->value * (sample->weight * mult);
        cache.derivative += (sample->value - prevSample.value) * 
                            ((prevSample->weight + sample->weight) / 2.0 * mult);
        
        if(additive) {
            cache.max = max(cache.max, sample->value);
            cache.min = min(cache.min, sample->value);
        } else {
            cache.dirty.range = true;
        }
        
        cache.dirty.variance = true;
        
        if(this->cycled()) {
            this->cascade()->push(cache.mean, cache.weight);
        }
    }
};

#ifndef MOD_HTORY_IMPL_H_2PASS
#define MOD_HTORY_IMPL_H_2PASS
#include "History.impl.h"
#else
#undef History
#undef History
#endif

#endif /* HTORY_H */

