#ifndef MOD_HISTOGRAM_H

#include "MIMod.h"
#include <vector>
#include "History.h"

#ifdef MOD_HISTOGRAM_H_2PASS
#   define MOD_HISTOGRAM_H
#   define _virt virtual
#   define Histogram VirtHistogram
#   define History VirtHistory
#else
#   define _virt 
#endif


namespace Modules {
    template<typename VALUE>
    class Histogram : public History<VALUE> {
    public:
        typedef VALUE HValue;
        typedef double HTime;
        typedef Histogram<VALUE> Self;
        typedef History<VALUE> SelfHistory;
        typedef unsigned int Idt;
        struct PDE {
            Num probability, density, error;
        };
        
        
        static constexpr const unsigned int MaxLocalAttractors = 32;
        
        struct Attractor {
            Self* histogram = nullptr;
            
            void allocate();
            bool valid() const;
            void invalidate();
            
            inline Self *const& operator->() {
                return histogram;
            }
        };
        typedef SmallMap<Attractor&, Num> Decomposition;
        
        inline const HValue& center() const {
            return SelfHistory::mean();
        }
        /*
        inline const HValue& sigma() const {
            return SelfHistory::sigma();
        }
        inline const Num& weight() const {
            return SelfHistory::weight();
        }*/
        
        //Value assumed to be & (~state)
        _virt void add(const HValue& state, const HValue& value, const Num& weight = 1.0);
        _virt void remove(const HValue& state, const HValue& value, const Num& weight = 1.0);
        _virt void update();
        _virt void frame();
        
        std::vector<Attractor> attractors;
        
        _virt HValue evaluate(const HValue& state, PDE *const& pdata = nullptr);
        _virt Decomposition decompose(const HValue& state, PDE *const& pdata = nullptr);
        
        _virt Num probability(const HValue& state);
        _virt Num density(const HValue& state);
        _virt Num error(const HValue& state);
        
        _virt Num significance(const HValue& state);
        _virt HValue consistence();
        
        _virt PDE pde(const HValue& val);
        
        _virt void clear();
    protected:
        struct AttractorWeights {
            Num weights[MaxLocalAttractors];
            Num total = 0.0;
            Idt count;
            inline Num& operator[](const Idt& idx) noexcept {
                return weights[idx];
            }
            inline const Num& operator[](const Idt& idx) const noexcept {
                return weights[idx];
            }
            void normalize(bool invert);
        };
        
        Num totalWeight;
        inline Num idealWeight() const;
        
        _virt AttractorWeights weights(const HValue& state);
        
        _virt Idt addAttr();
        _virt void mergeAttr(Idt a, Idt b);
        _virt void relaxAttr();
    };
};

#undef _virt

#ifndef MOD_HISTOGRAM_H_2PASS
#define MOD_HISTOGRAM_H_2PASS
#include "Histogram.h"
#else
#undef Histogram
#undef History
#endif

#endif /* HISTORY_H */
