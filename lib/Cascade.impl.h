#ifndef CASCADE_IMPL_H
#define CASCADE_IMPL_H

#include "Cascade.h"

#define CAS_TPL template<typename V>
#define CAS_T Cascade<V>

CAS_TPL
CAS_T::Cascade() : cscParent(nullptr), cascadeFactor(2.0), cascadeOffset(0), cscDepth(0), cscMaxDepth(6) {
    
}
CAS_TPL
CAS_T::~Cascade() {
    if(this->cscParent != nullptr) delete cscParent;
}

CAS_TPL
typename CAS_T::Self* CAS_T::cascade() {
    if(cscParent != nullptr) return cscParent;
    
    cscParent = new Self();
    cscParent->child = this;
    cscParent->cascadeFactor = cascadeFactor;
    cscParent->cscDepth = cscDepth+1;
    cscParent->cscMaxDepth = cscMaxDepth;
    
    return cscParent;
}

CAS_TPL
void CAS_T::clear() {
    if(this->cscParent != nullptr) delete cscParent;
    cscParent = nullptr;
}

#endif
