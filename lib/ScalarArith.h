#ifndef SCALARARITH_H
#define SCALARARITH_H
#include <type_traits>

#define SCL_TOK(x) x
#define SCL_CAT2(a,b) SCL_TOK(a b)
#define SCL_CAT(a,b) SCL_CAT2(SCL_TOK(a),SCL_TOK(b))
#define SCL_GEN_OP(OP) \
template<class=void>\
inline auto SCL_CAT(operator,OP)(const typename std::remove_pointer<PAR_T>::type & o) const {\
    const auto ov = o.as(self().type()).raw();\
    return self().val OP ov;\
}
#define SQNAME2(NM) NM##=
#define SQNAME(NM) SQNAME2(NM)
#define SCL_OP_EQ(OP) \
template<class=void>\
inline auto SQNAME(SCL_CAT(operator,OP))(const typename std::remove_pointer<PAR_T>::type o) {\
    self() = self() OP (o);\
    return self();\
}

namespace Modules {
    
    struct ScalarArith_Root {};

    template<typename PAR_T, typename VAL_T>
    class ScalarArith : public ScalarArith_Root {
    public:
        typedef PAR_T Parent;
        typedef VAL_T Value;
        //Parent parent;
        VAL_T val;
        
        template<class=void>
        inline auto self() const noexcept {
            return *reinterpret_cast<Parent>(this);
        }
        
        inline ScalarArith() : val(0.0) {};
        inline ScalarArith(const Value& v) : val(v) {};
        //inline ScalarArith(Parent par) : parent(par), val(0.0) {};
        //~ScalarArith();
        
        SCL_GEN_OP(+);
        SCL_GEN_OP(-);
        SCL_GEN_OP(*);
        SCL_GEN_OP(/);
        
        SCL_GEN_OP(==);
        SCL_GEN_OP(!=);
        SCL_GEN_OP(<);
        SCL_GEN_OP(>);
        SCL_GEN_OP(<=);
        SCL_GEN_OP(>=);
        
        //SCL_GEN_OP(&, ,;, false, true);
        //SCL_GEN_OP(|, ,;, true, false);
        
        SCL_OP_EQ(+);
        SCL_OP_EQ(-);
        SCL_OP_EQ(*);
        SCL_OP_EQ(/);
        //SCL_OP_EQ(&);
        //SCL_OP_EQ(|);
    private:

    };

};

#endif /* SCALARARITH_H */

