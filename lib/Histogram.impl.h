#include "Histogram.h"

#ifndef MOD_HISTOGRAM_IMPL_H
#ifdef MOD_HISTOGRAM_IMPL_H_2PASS
#define MOD_HISTOGRAM_IMPL_H
#define Histogram VirtHistogram
#define History VirtHistory
#endif

#define HIST_TPL template<typename VALUE>
#define HIST_T Histogram<VALUE>

namespace Modules {
    HIST_TPL void HIST_T::Attractor::allocate() {
        this->histogram = new HIST_T();
    }
    HIST_TPL bool HIST_T::Attractor::valid() const {
        return histogram != nullptr && histogram->weight() > 0.0;
    }
    HIST_TPL void HIST_T::Attractor::invalidate() {
        histogram->clear();
    }
    
    //Value assumed to be & (~state)
    HIST_TPL void HIST_T::add(const HValue& state, const HValue& value, const Num& weight) {
        SelfHistory::add(state, value, weight);
        
        
        
        AttractorWeights weights = this->weights(state);
        
        //Sigma being the reciprocal of normalized variance (ie, significance/informational content weight)
        const HValue sigmaMask = value.mask();
        HValue prediction, stateDelta, sigma = sigmaMask;
        
        for(Idt i = 0; i < attractors.size(); i++) {
            Attractor& attr = attractors[i];
            const Num recipWeight = 1.0 - weights[i];
            
            prediction += attr->evaluate(state) * weights[i];
            sigma = (attr->sigma | sigmaMask) * weights[i] + sigma * recipWeight;
            stateDelta += (attr->center - state) * weights[i];
        }
        
        HValue predictionDelta = ((prediction - value) * sigma);
        
        Num stateErr = stateDelta.magnitude();
        Num predictionErr = predictionDelta.magnitude();
        
        //Contains the significance of our prediction, 0...1, with 1 being fully predicted by current HDI
        Num predictionSig = sigma.magnitude();
        if(predictionSig <= 0.0) predictionSig = 0.0001;
        
        //Normalize the two errs somehow
        
        //Require both a difference in state and value (ie, possibility of new, non-conflicting information)
        //Then divide by the reciprocal of the certainty of the prediction, representing the degree of conflict with current system information
        Num systemErr = (stateErr * predictionErr) / (1.0 - predictionSig);
        Num systemUncertainty = (stateErr * predictionErr) / predictionSig;
        
        
    }
    
    HIST_TPL void HIST_T::remove(const HValue& state, const HValue& value, const Num& weight) {
        _unused(state);
        _unused(vale);
        _unused(weight);
    }
    HIST_TPL void HIST_T::update() {
        this->frame();
    }
    HIST_TPL void HIST_T::frame() {
        SelfHistory::frame();
    }
    
    HIST_TPL
    typename HIST_T::HValue HIST_T::evaluate(const typename HIST_T::HValue& state, typename HIST_T::PDE *const& pdata) {
        AttractorWeights weights = this->weights(state);
        HValue ret;
        for(Idt i = 0; i < attractors.size(); i++) {
            Attractor& attr = attractors[i];
            ret += attr->evaluate(state) * weights[i];
        }
        
        return ret;
    }
    HIST_TPL
    typename HIST_T::Decomposition HIST_T::decompose(const typename HIST_T::HValue& state, typename HIST_T::PDE *const& pdata) {
        AttractorWeights weights = this->weights(state);
        Decomposition ret;
        for(Idt i = 0; i < attractors.size(); i++) {
            Attractor& attr = attractors[i];
            ret[attr] = attr->evaluate(state) * weights[i];
        }
        
        return ret;
    }
    HIST_TPL
    Num HIST_T::probability(const typename HIST_T::HValue& state) {
        
    }
    HIST_TPL
    Num HIST_T::density(const typename HIST_T::HValue& state) {
        
    }
    HIST_TPL
    Num HIST_T::error(const typename HIST_T::HValue& state) {
        
    }
    
    HIST_TPL
    Num HIST_T::significance(const typename HIST_T::HValue& state) {
        
    }
    HIST_TPL
    typename HIST_T::HValue HIST_T::consistence() {
        
    }
    
    HIST_TPL
    typename HIST_T::PDE HIST_T::pde(const typename HIST_T::HValue& val) {
        
    }
    
    HIST_TPL void HIST_T::AttractorWeights::normalize(bool invert) {
        this->total = 0.0;
        for(Idt i = 0; i < MaxLocalAttractors; i++) {
            total +=  weights[i];
        }
        if(total == 0.0) return;
        
        for(Idt i = 0; i < MaxLocalAttractors; i++) {
            weights[i] /= total;
            if(invert) weights[i] = 1.0 - weights[i];
        }
    }
    
    HIST_TPL
    typename HIST_T::AttractorWeights HIST_T::weights(const typename HIST_T::HValue& state) {
        AttractorWeights ret;
        HValue sigmaMask = state.mask();
        
        for(Idt i = 0; i < attractors.size(); i++) {
            const Attractor& attr = attractors[i];
            Num dist = ((state - attr.center) * (attr.sigma | sigmaMask)).magnitude2();
            ret[i] = dist;
        }
        
        //Normalize and invert the squared distance
        ret.normalize(true);
        
        return ret;
    }
    
    HIST_TPL
    typename HIST_T::Idt HIST_T::addAttr() {
        
    }
    
    HIST_TPL void HIST_T::mergeAttr(typename HIST_T::Idt a, typename HIST_T::Idt b) {
        
    }
    HIST_TPL void HIST_T::relaxAttr() {
        
    }
    HIST_TPL void HIST_T::clear() {
        attractors.resize(0);
        SelfHistory::clear();
    }
};

#ifndef MOD_HISTOGRAM_IMPL_H_2PASS
#define MOD_HISTOGRAM_IMPL_H_2PASS
#include "Histogram.impl.h"
#else
#undef Histogram
#undef History
#endif

#endif /* HISTORY_H */
