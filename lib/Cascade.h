#ifndef CASCADE_VECTOR_H
#define CASCADE_VECTOR_H
#include "GenIterator.h"
#include "MIMod.h"

template<typename V>
class Cascade {
public:
    typedef Cascade<V> Self;
    typedef V Value;
    
    Cascade();
    ~Cascade();
    
    Self* cascade();
    
    void clear();
protected:
    Self *cscParent;
    Num cascadeFactor;
    Num cascadeOffset;
    unsigned short cscDepth, cscMaxDepth;
public:
    
};

#endif

