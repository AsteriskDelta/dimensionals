#ifndef CYCLIC_IMPL_H
#define CYCLIC_IMPL_H

#include "CyclicVector.h"

#define CV_TPL template<typename V>
#define CV_T CyclicVector<V>

CV_TPL
CV_T::CyclicVector() : base(), edge() {
    
}
CV_TPL
CV_T::CyclicVector(typename CV_T::Idt sz) : base(), edge() {
    this->setMaxSize(sz);
}

CV_TPL
inline typename CV_T::Idx CV_T::resolveIdt(const typename CV_T::Idt& idt) const {
    return Idx(idt % this->size());
}

CV_TPL
const typename CV_T::Value& CV_T::get(const typename CV_T::Idt& id) const {
    return base[this->resolveIdt(id)];
}
CV_TPL
const typename CV_T::Value& CV_T::rawGet(const typename CV_T::Idx& absID) const {
    return base[absID];
}

CV_TPL
bool CV_T::cycled() const {
    return edge.fill > 0 && edge.leading == 0;
}

CV_TPL
typename CV_T::Idt CV_T::push(const typename CV_T::Value& val) {
    if(edge.fill < edge.max) {
        base.push_back(val);
    } else {
        base[edge.leading] = val;
        edge.trailing = (edge.leading + 1) % edge.max;
    }
    
    const Idx ret = edge.leading;
    edge.leading = (edge.leading + 1) % edge.max;
    edge.fill = std::min(int(edge.fill + 1), int(edge.max));
    return ret;
}
CV_TPL
typename CV_T::Idt CV_T::pop() {
    if(edge.fill <= 0) return 0;
    
    edge.leading = (edge.leading - 1) % edge.max;
    edge.fill--;
    return (edge.leading - 1) % edge.max;
}

CV_TPL
typename CV_T::Idt CV_T::size() const {
    return std::min(edge.fill, edge.max);
}
CV_TPL
typename CV_T::Idt CV_T::maxSize() const {
    return edge.max;
}
CV_TPL
void CV_T::setMaxSize(const typename CV_T::Idx& sz) {
    base.reserve(sz);
    edge.max = sz;
}

CV_TPL
void CV_T::clear() {
    base.clear();
    edge = decltype(edge)();
}

CV_TPL
typename CV_T::iterator CV_T::begin() const {
    return iterator(this, edge.trailing);
}
CV_TPL
typename CV_T::iterator CV_T::end() const {
    return iterator(this, edge.leading);
}
CV_TPL
typename CV_T::iterator CV_T::invalid() const {
    return iterator(nullptr, 0);
}

#endif
