#ifndef VALHISTORY_H
#define VALHISTORY_H
#include "MIMod.h"
#include "History.h"

namespace Modules {
    using ValHistory= History<Num>;
}

#endif /* VALHISTORY_H */

