#ifndef SMALLMAP_H
#define SMALLMAP_H
#include "MIMod.h"
#include "GenIterator.h"

template<typename KEY, typename VALUE>
class SmallMap {
public:
    typedef unsigned short Idt;
    typedef KEY Key;
    typedef VALUE Value;
    typedef SmallMap<KEY,VALUE> Self;
    
    SmallMap();
    SmallMap(const SmallMap& orig);
    ~SmallMap();
    
    bool has(const Key& key) const noexcept;
    const Value& at(const Key& key) const;
    Value& at(const Key& key, const bool& create = false);
    
    inline const Value& operator[](const Key& key) const {
        return this->at(key);
    }
    inline Value& operator[](const Key& key) {
        return this->at(key, true);
    }
    
    struct Pair {
        Key key;
        Value value;
        bool valid() const;
        void invalidate();
    };
    typedef const Pair ConstPair;
    
    const Pair* get(const Key& key) const;
    Pair* get(const Key& key, const bool& create = false);
    
    struct iterator : public GenIterator<Pair> {
        std::vector<Pair> *vec;
        Idt idx;
        
        inline iterator(const std::vector<Pair> *const& v, const Idt& i) :
        vec(const_cast<std::vector<Pair> *const>(v)), idx(i) {};
        operator Key&() const;
        
        void advance();
        void retreat();
        //bool equal(const iterator& o) const noexcept override;
        Pair* ptr() const;
        
    };
    
    bool erase(const Key& key);
    Pair* insert(const Key& key, const Value& val, const bool& force = false);
    
    inline size_t size() const noexcept {
        return values.size() - unused.size();
    }
    inline bool empty() const noexcept {
        return this->size() == 0;
    }
    inline size_t allocated() const noexcept {
        return values.size();
    }
    
    iterator begin() const;
    iterator end() const;
    iterator iter(const Key& key) const;
    
    inline Idt getID(const Pair *const& pair) const;
private:
    
    std::vector<Pair> values;
    std::vector<Idt> unused;
    static Value nilValue;
};

#endif /* SMALLMAP_H */

