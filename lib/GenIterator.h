#ifndef GENITERATOR_H
#define GENITERATOR_H
#include <vector>

template<typename VALUE>
class GenIterator : public std::iterator<std::bidirectional_iterator_tag, VALUE> {
public:
    typedef GenIterator<VALUE> Self;
    typedef VALUE Value;

    /*inline GenIterator() {
            
    }*/

    /*inline GenIterator(const GenIterator& orig) {
        _unused(orig);
    }*/
    inline ~GenIterator() {

    }

    inline explicit operator bool() const noexcept {
        return this->valid();
    }

    inline Value* operator->() const {
        return this->ptr();
    }

    inline Value& operator*() const {
        return *(this->operator->());
    }

    inline Self& operator++() {
        this->advance();
        return *this;
    }

    inline Self& operator--() {
        this->retreat();
        return *this;
    }

    inline bool operator==(const Self& o) const noexcept {
        return this->equal(o);
    }

    inline bool operator!=(const Self& o) const noexcept {
        return !(this->equal(o));
    }

    //GenIterator next() const;
    //GenIterator prev() const;

    inline bool valid() const noexcept {
        return this->ptr() != nullptr;
    }

protected:

    inline void advance() {

    }

    inline void retreat() {

    }

    inline bool equal(const Self& o) const noexcept {
        return this->ptr() == o.ptr();
    }

    inline Value* ptr() const {
        return nullptr;
    }
private:

};

#endif /* GENITERATOR_H */

